# Panagenda

It aims to convert from different agenda platforms that focus on activism and self-organized events. The idea is to use an internal format so that each standard needs to be implemented just read-wise and write-wise, to and from this internal format.

## Formats to support

* Internet calendar (ical, ics)
* Activismap API (activismap.net)
* Radar API (radar.squat.net)

## API and standard references

* Icalendar
    * RFC 2445. http://www.kanzaki.com/docs/ical/
    * Python's icalendar. https://icalendar.readthedocs.io/en/latest/usage.html
* Activismap
    * https://activismap-api.owldevelopers.tk/doc/index.html
* Radar
    * Startpoint: https://0xacab.org/radar/drupal-make/wikis/api/1.1/notes
    * PHP API code: https://0xacab.org/radar/radar-api-php/tree/master/src/Entity

## Requirements

* **configparser**: to read credentials from a configuration file
* **requests**: to use REST API's
* **bs4**: to extract clean text from html rich-formatted descriptions from Radar
* **datetime, pytz**: to manage time of the activities
* **icalendar**: to write/read icalendar files

## General to-do's

* Refine the package / repo file structure so that it becomes a pip-conformat python package
* Manage errors, you clumsy monkey
