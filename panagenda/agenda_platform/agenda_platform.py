# TODO: consider to split on to multiple mixins or traits
# TODO: enforce input and output types
from panagenda.panagenda import Activity, Group, Place


class Platform(object):

    url_domain = None

    # Read from agenda_platform and return a Panagenda object list
    @staticmethod
    def read_activities(self) -> [Activity]:
        raise NotImplementedError("must implement "+__name__)

    @staticmethod
    def read_groups(self) -> [Group]:
        raise NotImplementedError("must implement " + __name__)

    @staticmethod
    def read_places(self) -> [Place]:
        raise NotImplementedError("must implement " + __name__)

    # Take a Panagenda object and write it to agenda_platform
    @staticmethod
    def write_activities(activities: [Activity]):
        raise NotImplementedError("must implement " + __name__)

    @staticmethod
    def write_groups(groups: [Group]):
        raise NotImplementedError("must implement " + __name__)

    @staticmethod
    def write_places(places: [Group]):
        raise NotImplementedError("must implement " + __name__)
