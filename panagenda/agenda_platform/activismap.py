from panagenda.agenda_platform.agenda_platform import Platform
from panagenda.panagenda import Activity, Place, Group
from datetime import datetime
import pytz
import requests
from configparser import RawConfigParser
import os
from enum import Enum


class Format(Enum):
    ASSEMBLY = 0
    TALKING = 1
    FESTIVAL = 2
    POPULAR_PARTY = 3
    LEARNING = 4
    STRIKE = 5
    KAFETA = 6
    MANIFESTATION = 7
    MARKET = 8
    OTHER = 9999


class Topic(Enum):
    AMNESTY = 0
    ANTICAPITALISM = 1
    ANTIFASCISM = 2
    ANTIMILITARISM = 3
    FAIR_TRADE = 4
    COOPERATIVE = 5
    EDUCATION = 6
    TERRITORY_DEFENSE = 7
    ECOLOGY = 8
    FEMINISM = 9
    INTEGRATION = 10
    TREATED = 11
    MEMORY = 12
    ASYLUM = 13
    OCCUPATION = 14
    POLICY = 15
    HEALTH = 16
    TRADE_UNIONISM = 17
    RURAL_WORLD = 18
    NATIONAL_LIBERTY = 19
    SOLIDARITY = 20
    INTERNET = 21
    ANIMAL_MOV = 22
    OTHER = 9999


def flatten_str(s: str) -> str:
    def parse_char(c):
        if c.isupper():
            c = parse_char(c.lower())
        if c is " ":
            c = parse_char("_")
        if c.isalnum() or \
           c is '.'or \
           c is '_' or \
           c is '+' or \
           c is '-':
            return c
        else:
            return '.'

    chars = []
    for c in s:
        chars += parse_char(c)

    return "".join(chars)


def str2email(s: str) -> str:
    return flatten_str(s) + '@' + 'panagenda.net'


class Activismap(Platform):

    url_domain = "activismap.net"

    @staticmethod
    def build_id(raw_id):
        return str(raw_id) + '@' + Activismap.url_domain

    @staticmethod
    def write_places(places: [Group]):
        pass

    @staticmethod
    def read_places(self) -> [Place]:
        pass

    @staticmethod
    def write_group(group: Group) -> str:
        url = 'https://activismap-api.owldevelopers.tk/v1/company'
        token = Activismap.login()
        name = group.name
        email = group.email if group.email else str2email(group.name)
        r = requests.post(url,
                          data={'name': name,
                                'email': email},
                          headers={'Authorization': 'Bearer ' + token})
        print(r.text)
        return r.json()['data']['id']

    @staticmethod
    def write_groups(groups: [Group]):
        for group in groups:
            Activismap.write_group(group)

    activities_url = "https://activismap-api.owldevelopers.tk/v1/public/search"

    @staticmethod
    def read_activities():
        response = requests.get(Activismap.activities_url)
        print(response.text)
        activities = []
        for entry in response.json()['data']:
            print("ENTRY: {}\n".format(entry))
            uid = Activismap.build_id(entry['id'])
            title = entry['title']
            description = entry['description']
            place = Place('place', None)
            poster = None
            if 'image' in entry:
                poster = entry['image']
            dt_start = datetime.fromtimestamp(int(entry['start_date'])/1000, pytz.utc)
            dt_end = datetime.fromtimestamp(int(entry['end_date'])/1000, pytz.utc)
            _format = entry['type']
            topics = entry['categories']
            name = entry['company']['name']
            _id = Activismap.build_id(entry['company']['id'])
            email = entry['company']['email']
            organizer = Group(name, _id, email)

            activity = Activity(organizer, place, dt_start, dt_end, _format,
                                topics, title, description, poster, None, uid)
            activities.append(activity)
        return activities

    @staticmethod
    def login():
        config = RawConfigParser()
        package_dir = os.path.dirname(os.path.abspath(__file__))
        filename = os.path.join(package_dir, 'agenda_platform.cfg')
        configfile = open(filename, 'r')
        config.read_file(configfile)
        client_id = config.get('Activismap', 'client_id')
        client_secret = config.get('Activismap', 'client_secret')
        username = config.get('Activismap', 'username')
        password = config.get('Activismap', 'password')
        print("Config:", client_id, client_secret, username, password)

        # if parameters are wrong, return 'ᕕ( ᐛ )ᕗ'

        url = "https://activismap-api.owldevelopers.tk/oauth/v2/token"
        grant_type = "password"
        content = {
                'client_id': client_id,
                'client_secret': client_secret,
                'username': username,
                'password': password,
                'grant_type': grant_type
            }
        print("Request data: ", content)
        r = requests.post(url, data=content)
        print("Response:", r.text)
        j = r.json()
        access_token = j['access_token']

        return access_token

    @staticmethod
    def write_activity(activity, company_id, token):

        def dt_to_msts(dt):
            return int(dt.timestamp() * 1000)

        url = 'https://activismap-api.owldevelopers.tk/v1/event'

        data = {
            'title': activity.title,
            'description': activity.description,
            'start_date': dt_to_msts(activity.dt_start),
            'end_date': dt_to_msts(activity.dt_end),
            'lat': activity.place.coords.lat,
            'lon': activity.place.coords.long,
            # TODO: extract from data
            'categories': 'HEALTH',
            # TODO: extract from data
            'type': 'STRIKE',
            'company_id': company_id,
        }
        auth_header = {'Authorization': 'Bearer ' + token}
        r = requests.post(url, data, headers=auth_header)
        print(r.text)

    @staticmethod
    def write_activities(activities):

        token = Activismap.login()
        groups_dict = Activismap.read_my_groups()
        for activity in activities:
            # Check by company name, if this group is registered
            name = flatten_str(activity.group.name)
            if name not in groups_dict:
                # create new "company"
                company_id = Activismap.write_group(activity.group)
                company_id = Activismap.build_id(company_id)
            else:
                # use existing "company id"
                full_id = groups_dict[name].id
                company_id = full_id.split('@')[0]
            # Register new event with the valid company_id
            Activismap.write_activity(activity, company_id, token)

    @staticmethod
    def read_groups() -> [Group]:
        token = Activismap.login()
        r = requests.get("https://activismap-api.owldevelopers.tk/v1/user/companies",
                         headers={'Authorization': 'Bearer ' + token})
        print(r.text)
        groups = []
        for company in r.json()['data']:
            # todo: read also extra links, like websites
            groups.append(Group(company['name'],
                                Activismap.build_id(company['id']),
                                company['email'],
                                ))

        return groups

    @staticmethod
    def build_groups_dict(groups: [Group]) -> {str: Group}:
        _dict = {}
        for group in groups:
            _dict[flatten_str(group.name)] = group

        return _dict

    @staticmethod
    def read_my_groups() -> {str: Group}:
        return Activismap.build_groups_dict(Activismap.read_groups())

