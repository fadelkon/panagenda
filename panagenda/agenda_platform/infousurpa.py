from panagenda.agenda_platform.agenda_platform import Platform
from panagenda.panagenda import Place, Group, Activity
from datetime import datetime, timedelta
import pytz

from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle, Image, Table
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib import units, colors
from enum import Enum

class w(Enum):
    MON = 0,
    TUE = 1,
    WED = 2,
    THU = 3,
    FRI = 4,
    SAT = 5,
    SUN = 6

class Zone(Enum):
    """
    Logical separations on the pdf.
    
    Most of them are geographic, but are quite arbitrary.
    """

    BCN_SURROUND = 0
    """ZONA DEL BAIX LLOBREGAT – GARRAF - BARCELONÈS"""

    BCN_SANTS = 1
    """SANTS – HOSTAFRANCS – LA BORDETA – BADAL"""

    BCN_GRACIA = 2
    """GRÀCIA – VALLCARCA – LA SALUT"""

    BCN_CLOT = 3
    """SAGRADA FAMÍLIA – CLOT – CAMP DE L'ARPA- FORT PIENC"""

    BCN_ST_ANDREU = 4
    """POBLE NOU – SAGRERA – SANT ANDREU"""

    BCN_HORTA_9_BARRIS = 5
    """LA TAXONERA - EL CARMEL - HORTA – GUINARDÓ – NOU BARRIS"""

    BCN_ST_ANTONI = 6
    """SANT ANTONI – POBLE SEC – EIXAMPLE ESQUERRE"""

    BCN_CENTER = 7
    """RAVAL – CASC ANTIC"""

    COLLSEROLA = 8
    """COLLSEROLA – SARRIÀ"""

    BESOS = 9
    """BESÒS – SANTA COLOMA – BADALONA"""

    BCN_PROVINCE = 10
    """VALLÈS – BAGES – MARESME – ALTRES"""

    OTHER_CAT = 11
    """TARRAGONA – LLEIDA – GIRONA"""

    UNI = 12
    """UNIVERSITATS LLIURES"""


styles = getSampleStyleSheet()

pagesize = landscape(A4)
PAGE_HEIGHT = pagesize[1]
PAGE_WIDTH = pagesize[0]
PAGE_MARGIN = units.inch/3
CONTENT_WIDTH = PAGE_WIDTH - PAGE_MARGIN*2

class InfoUsurpa(Platform):

    @staticmethod
    def read_places(self) -> [Place]:
        raise NotImplementedError("this platform is write-only")

    @staticmethod
    def read_activities(self) -> [Activity]:
        raise NotImplementedError("this platform is write-only")

    @staticmethod
    def read_groups(self) -> [Group]:
        raise NotImplementedError("this platform is write-only")


    # TODO: guess how to get the custom "zone" from address or coords
    @staticmethod
    def place_to_zone(place):
        raise NotImplementedError("must implement "+__name__)

    @staticmethod
    def group_to_zone(place):
        raise NotImplementedError("must implement "+__name__)

    @staticmethod
    def write_pdf(zones, places, filename):
        doc = SimpleDocTemplate(filename,
                                pagesize=pagesize,
                                leftMargin=PAGE_MARGIN,
                                rightMargin=PAGE_MARGIN,
                                topMargin=PAGE_MARGIN,
                                bottomMargin=PAGE_MARGIN
                                )

        story = []

        def resizeImage(img: Image, page_r) -> Image:
            h = img.drawHeight
            w = img.drawWidth
            r = h / w
            img.drawHeight = page_r * PAGE_HEIGHT
            img.drawWidth = img.drawHeight / r
            return img

        ##########
        # Header #
        ##########

        header1 = resizeImage(Image('../resources/infousurpa/info.jpg'), 0.2)
        header2 = resizeImage(Image('../resources/infousurpa/auca.jpg'), 0.2)
        header3 = resizeImage(Image('../resources/infousurpa/usurpa.jpg'), 0.2)

        header_content = [[header1, header2, header3]]
        center_cell_width = header2.drawWidth
        side_cells_width = (CONTENT_WIDTH - center_cell_width) / 2

        header_style = TableStyle([('ALIGN', (0, 0), (-1, -1), 'CENTER')])
        header_table = Table(header_content,
                             colWidths=[side_cells_width, center_cell_width, side_cells_width],
                             )
        header_table.setStyle(header_style)

        #############
        # Top label #
        #############

        label_data = [['::: Butlletí setmanal de contr@informació des del 1996',
                       'Núm. XXX setmana del YY al ZZ d’abril de 2017 :::']]
        label_style = TableStyle([('BACKGROUND', (0, 0), (-1, -1), colors.black),
                                  ('TEXTCOLOR', (0, 0), (-1, -1), colors.white),
                                  ('ALIGN', (0, 0), (0, 0), 'LEFT'),
                                  ('ALIGN', (-1, -1), (-1, -1), 'RIGHT'),
                                  ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                                  ('BOX', (0, 0), (-1, -1), 0.25, colors.black)
                                  ])

        label_table = Table(label_data,
                            colWidths=[CONTENT_WIDTH / 2, CONTENT_WIDTH / 2])
        label_table.setStyle(label_style)

        ##############
        # Main table #
        ##############

        # Common
        # ------

        main_table_style_raw = [
            # subtitle
            ('ALIGN', (0, 0), (-1, 0), 'CENTER'),
            ('SPAN', (0, 0), (-1, 0)),
            # weekdays
            ('BACKGROUND', (0, 1), (-1, 1), colors.black),
            ('TEXTCOLOR', (0, 1), (-1, 1), colors.white),
            ('ALIGN', (0, 1), (-1, 1), 'CENTER'),
        ]
        main_table_data = []

        # Subtitle
        # --------

        # subtitle_style = styles['BodyText']
        import reportlab.lib.enums as enums
        subtitle_style = ParagraphStyle(alignment=enums.TA_CENTER, name='subtitle')
        subtitle_data = [
            Paragraph(
                '<para><b>L\'INFO USURPA TORNA ALS CARRERS! PASSA\'L A BUSCAR PER:</b><br/>\
                AT. LLIB. GRACIA - EL LOKAL – AT. LLIB. PALOMAR – CAN VIES<br/>\
                <font size="10">SI FALTEN CÒPIES escriviu-nos a: usurpa@riseup.net</font></para>',
                style=subtitle_style
            ),
            '', '', '', '', '', '', ''
        ]
        main_table_data += [subtitle_data]

        # Weekdays
        # --------

        weekdays_data = ['', 'dimecres', 'dijous', 'divendres',
                         'dissabte', 'diumenge', 'dilluns', 'dimarts']
        main_table_data += [weekdays_data]

        # Dynamic content
        # ---------------
        group_style = ParagraphStyle(alignment=enums.TA_LEFT, name='group')

        row_idx = 1
        for zone in zones:
            row_idx += 1
            zone_data = [zone['name'], '', '', '', '', '', '', '', ]
            main_table_data += [zone_data]
            main_table_style_raw += [
                ('ALIGN', (0, row_idx), (-1, row_idx), 'CENTER'),
                ('BACKGROUND', (0, row_idx), (-1, row_idx), colors.black),
                ('TEXTCOLOR', (0, row_idx), (-1, row_idx), colors.white),
                ('SPAN', (0, row_idx), (-1, row_idx))]

            for place_name in zone['places']:
                row_idx += 1
                main_table_style_raw += [
                    ('VALIGN', (1, row_idx), (-1, row_idx), 'TOP')
                ]
                place = places[place_name]
                place_data = [Paragraph(
                    '<para>' +
                    '<b>' + place['name'] + '</b><br/>' +
                    place['addr'] +
                    '</para>',
                    style=group_style
                )]
                events = place['events']
                for weekday in [w.WED, w.THU, w.FRI, w.SAT, w.SUN, w.MON, w.TUE]:
                    if weekday in events:
                        dayevents = ''
                        for dayevent in events[weekday]:
                            dayevents_text = ""
                            dayevents_text = dayevents_text + dayevent['time'] + ': ' + \
                                             '<b>' + dayevent['name'] + '</b><br/>'
                        dayevents_para = Paragraph(
                            '<para>' + dayevents_text + '</para>',
                            style=group_style
                        )
                    else:
                        dayevents_para = ''
                    place_data += [dayevents_para]
                main_table_data += [place_data]

        ###############
        # Concat Main #
        ###############

        main_table_style_raw += [
            # global
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ]
        main_table_style = TableStyle(main_table_style_raw)

        main_table = Table(data=main_table_data,
                           colWidths=8 * [CONTENT_WIDTH / 8])
        main_table.setStyle(main_table_style)

        #########
        # Build #
        #########

        story.append(header_table)
        story.append(label_table)
        story.append(main_table)

        doc.build(story)

    @staticmethod
    def write_week_activities(activities):
        pass

    @staticmethod
    def write_activities(activities):

        def next_tuesday(mydatetime: datetime) -> datetime:
            tuesday = 1
            diff_days = (tuesday - now.weekday()) % 7
            # next tue, same time as now
            then = mydatetime + timedelta(days=diff_days)
            # next tue, at default time: 00:00
            next_td = datetime(then.year,
                               then.month,
                               then.day)
            return next_td

        week_activities = []
        now = datetime.now(pytz.utc)
        start = next_tuesday(now)
        end = start + timedelta(days=7)
        for activity in activities:
            if activity.dt_start >= start and activity.dt_end < end:
                week_activities.append(activity)

        InfoUsurpa.write_activities(week_activities)
