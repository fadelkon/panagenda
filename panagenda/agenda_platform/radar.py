
# groups
# https://radar.squat.net/api/1.1/search/groups.json?facets[country][]=XC
#
# events
# https://radar.squat.net/api/1.1/search/events.json?facets[city][]=Barcelona
# https://radar.squat.net/api/1.1/search/events.json?facets[group][]=41
from panagenda.agenda_platform.agenda_platform import Platform
import requests
from panagenda.panagenda import Place, Group, Activity, Link
import iso8601
from bs4 import BeautifulSoup
from enum import Enum

"""
Enum of activity formats based representing Radar's category taxonomy terms
source: https://0xacab.org/radar/drupal-make/blob/7.x-2.x/modules/radar/radar_topics/radar_topics.features.uuid_term.inc
"""
class Format(Enum):
    MUSIC = 0
    """music"""

    DIY = 1
    """work space/diy"""

    BOOKS = 2
    """book shop/info shop/library"""

    OFFICE = 3
    """advice/help/office hours"""

    PROTEST = 4
    """action/protest/camp"""

    PARTY = 5
    """party"""

    FOOD = 6
    """food"""

    DISCUSSION = 7
    """discussion/presentation"""

    FILM = 8
    """film"""

    EXHIBITION = 9
    """exhibition"""

    THEATER = 10
    """theater"""

    WORKSHOP = 11
    """course/workshop"""

    BAR = 12
    """bar/cafe"""

    MEETING = 13
    """meeting"""

    FREESHOP = 14
    """free shop"""

    OTHER = 9999

formats = {
    """
    Map of taxonomy elements for categories of elements
    source: https://0xacab.org/radar/drupal-make/blob/7.x-2.x/modules/radar/radar_topics/radar_topics.features.uuid_term.inc
    """
    
    "ff3e2872-6140-4645-98f0-784d656a9c5c": Format.MUSIC,
    "fc1d0760-6f0c-4239-be54-00ba7b97b965": Format.DIY,
    "edad891e-151a-4219-9822-39c7dcff6a4f": Format.BOOKS,
    "e97f372b-29bc-460b-bff6-35d2462411ff": Format.OFFICE,
    "e85a688d-03ac-4008-a3cb-1adb7e8f718a": Format.PROTEST,
    "b89e871f-c923-4f66-a7ba-a34bc9ccce5b": Format.PARTY,
    "8e846372-fa86-4cb2-87d1-f24da784ec6b": Format.FOOD,
    "8463bb01-e974-4785-9c2d-b95d87c9ee2d": Format.DISCUSSION,
    "68197b93-2ece-4b0f-9a76-d9e99bda2603": Format.FILM,
    "5ef858bd-178c-4d89-9a96-742b0e3f250f": Format.EXHIBITION,
    "3a8606d4-95ac-4941-a2ae-05835e5903dc": Format.THEATER,
    "2a7f6975-4c01-4777-8611-dffe0306c06f": Format.WORKSHOP,
    "2a56c4d7-eb98-4f96-9ac6-d383a1af5ce8": Format.BAR,
    "20a888f9-54c1-4767-8af1-40de3d1d2636": Format.MEETING,
    "0b9e8d1f-d51d-4d32-b984-2dba1099e0fa": Format.FREESHOP
}


class Radar(Platform):

    url_domain = "radar.squat.net"

    @staticmethod
    def build_id(raw_id):
        return str(raw_id) + '@' + Radar.url_domain

    @staticmethod
    def read_groups(self) -> [Group]:
        pass

    @staticmethod
    def write_activities(activities: [Activity]):
        pass

    @staticmethod
    def write_places(places: [Group]):
        pass

    @staticmethod
    def read_places(self) -> [Place]:
        pass

    @staticmethod
    def write_groups(groups: [Group]):
        pass

    activities_url = "https://radar.squat.net/api/1.1/search/events.json"
    activities_params = ["title", "offline", "image", "topic",
                         "date_time", "og_group_ref", "uuid", "topic"]
    activities_param_str = "fields[]=" + "&fields[]=".join(activities_params)

    radar_places = {}
    radar_groups = {}

    def read_activities_facet(self, facet_name, facet_value):
        def resolve_place(place_id, place_uri):
            if place_id in self.radar_places:
                return self.radar_places[place_id]
            response = requests.get(place_uri + '.json')
            p = response.json()

            coords = Place.Coords(p['map']['lat'], p['map']['lon'])
            a = p['address']
            name = a['name_line']
            address = a['thoroughfare'] + ', ' + a['locality']
            place = Place(name, coords, address)
            self.radar_places[place_id] = place
            return place

        def resolve_group(group_id, group_uri):
            if group_id in self.radar_groups:
                return self.radar_groups[group_id]
            response = requests.get(group_uri + '.json')
            g = response.json()

            name = g['title']
            links = []
            links.append(Link(g['url'], Link.Platform.RADAR, Link.Content.IDENTITY))
            email = g['email']
            group = Group(name, Radar.build_id(group_id), email, links)
            self.radar_groups[group_id] = group
            return group

        url = self.activities_url + "?facets[" + facet_name + "][]=" + facet_value +\
            "&" + self.activities_param_str
        response = requests.get(url)

        # debug: save response json
        file = open("radar-events-bcn.json", "w")
        file.write(response.text)

        activities = []
        entries = response.json()['result']
        for entry in entries:
            entry = entries[entry]
            print("ENTRY: {}\n".format(entry))
            uid = Radar.build_id(entry['uuid'])
            title = entry['title']

            if 'value' in entry['body']:
                if 'format' in entry['body'] and entry['body']['format'] == "rich_text_editor":
                    html = BeautifulSoup(entry['body']['value'], 'html.parser')
                    description = html.get_text()
                else:
                    description = entry['body']['value']
            else:
                description = None

            # todo: manage case of multiple places
            place_id = place_uri = None
            for resource in entry['offline']:
                if resource['resource'] != 'location':
                    continue
                place_uri = resource['uri']
                place_id = resource['id']
            place = Place("place", None)
            if place_uri is not None and place_id is not None:
                place = resolve_place(place_id, place_uri)

            # radar doesn't output images through api
            poster = None

            dt_start = iso8601.parse_date(entry['date_time'][0]['time_start'])
            dt_end = iso8601.parse_date(entry['date_time'][0]['time_end'])

            # todo: resolve format from 'category'
            _format = None

            topics = entry['topic']

            # todo: manage case of multiple organizer groups
            for resource in entry['og_group_ref']:
                if resource['resource'] != 'node':
                    continue
                node_uri = resource['uri']
                node_id = resource['id']
            organizer = Group("Organizer", Radar.build_id("123456"))
            if node_uri is not None and node_id is not None:
                organizer = resolve_group(node_id, node_uri)

            activity = Activity(organizer, place, dt_start, dt_end, _format,
                                topics, title, description, poster, None, uid)
            activities.append(activity)
        return activities

    def read_activities_city(self, city="Barcelona"):
        return self.read_activities_facet("city", city)

    def read_activities(self):
        return self.read_activities_city("Barcelona")
