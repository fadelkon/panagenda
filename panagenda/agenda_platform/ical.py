from icalendar import Event, Calendar
from icalendar.prop import vText, vCalAddress
from panagenda.agenda_platform.agenda_platform import Platform
from panagenda.panagenda import Place, Group, Activity
from datetime import datetime
import pytz


# TODO: read/write format and topic from/to CATEGORIES
class Icalendar(Platform):
    @staticmethod
    def write_event(activity):
        event = Event()

        event['location'] = vText(activity.place.name)

        if activity.group.email is None:
            organizer = vCalAddress('MAILTO:info@activismap.net')
        else:
            organizer = vCalAddress('MAILTO:'+activity.group.email)
        organizer.params['cn'] = vText(activity.group.name)
        event['organizer'] = organizer

        attendee = organizer
        attendee.params['role'] = vText('REQ-PARTICIPANT')
        attendee.params['partstat'] = vText('ACCEPTED')

        event['summary'] = activity.title
        if activity.description is not None:
            event['description'] = activity.description
        event.add('dtstamp', datetime.now(pytz.utc))
        # dt = datetime.now(pytz.utc)
        event.add('dtstart', activity.dt_start)
        if activity.dt_end is not None:
            event.add('dtend', activity.dt_end)
        # event['uid'] = '20050115T101010/27346262376@activismap.org'
        event['uid'] = activity.uid
        return event

    @staticmethod
    def write_activities(activities):
        cal = Calendar()
        cal['prodid'] = '-//panagenda//activismap.org//'
        cal['version'] = '2.0'
        for activity in activities:
            cal.add_component(Icalendar.write_event(activity))
        return cal.to_ical()

    @staticmethod
    def read_calendar(str_calendar):
        cal = Calendar()
        cal = Calendar.from_ical(str_calendar)

        events = []
        for component in cal.subcomponents:
            # print(component)
            if not isinstance(component, Event):
                continue
            events.append(component)
            print(component)

        return events

    @staticmethod
    def read_activities(str_calendar):
        events = Icalendar.read_calendar(str_calendar)
        activities = []

        def prop_str(_str):
            return event.decoded(_str).decode("utf-8")

        for event in events:
            uid = prop_str('uid')
            title = prop_str('summary')
            description = prop_str('description')
            place = Place(prop_str('location'), None)
            poster = None
            dt_start = event.decoded('dtstart')
            dt_end = event.decoded('dtend')
            _format = ""
            topics = ""
            group_name = event.decoded('organizer').replace("MAILTO:", "")
            group_id = event.decoded('id')
            organizer = Group(group_name, group_id, None)

            activity = Activity(organizer, place, dt_start, dt_end, _format,
                                topics, title, description, poster, None, uid)
            print(activity)
            activities.append(activity)

        return activities
