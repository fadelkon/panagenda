from enum import Enum


class Link(object):
    class Platform(Enum):
        FACEBOOK   = 0
        TWITTER    = 1
        BLOG       = 2
        ACTIVISMAP = 3
        RADAR      = 4
        GNUSOCIAL  = 5
        DIASPORA   = 6
        PUMPIO     = 7
        HUBZILLA   = 8
        ICAL       = 9
        OTHER      = 99

    class Content(Enum):
        ACTIVITY = 0
        IDENTITY = 1
        PLACE = 2
        OTHER = 99

    def __init__(self, url, platform = Platform.OTHER, entity = Content.OTHER):
        self.url = url
        self.platform = platform
        self.entity = entity

# TODO: should rename "format"
# TODO: group and place could be tuples or even be split to multiple attributes each one
class Activity(object):
    def __init__(self, group, place, dt_start, dt_end,
                 format, topic, title, description,
                 poster, links, uid):
        self.group = group
        self.place = place
        self.dt_start = dt_start
        self.dt_end = dt_end
        self.format = format
        self.topic = topic
        self.title = title
        self.description = description
        self.poster = poster
        self.links = links
        self.uid = uid

    def __str__(self):
        str_list = ["Activity: { "]
        for key in [attr for attr in dir(self) if not callable(attr) and not attr.__contains__("__")]:
            str_list.append(key)
            str_list.append(": ")
            str_list.append(str(self.__dict__[key]))
            str_list.append("; ")
        str_list.append(" }")
        return "".join(str_list)


class Group(object):
    def __init__(self, name, _id, email=None, links=None):
        self.name = name
        self.links = links
        self.email = email
        self.id = _id


class Place(object):
    class Coords(object):
        def __init__(self, lat, long):
            self.lat = lat
            self.long = long

    def __init__(self, name, links, coords=None, address=None):
        self.name = name
        self.links = links
        self.coords = coords
        self.address = address
