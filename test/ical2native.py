from panagenda.agenda_platform.ical import Icalendar

#######################
# Test ical to native #
#######################

file = open('test.ics', 'rb')
str_ics = file.read()
activities = Icalendar.read_activities(str_ics)
for activity in activities:
    print(activity)
