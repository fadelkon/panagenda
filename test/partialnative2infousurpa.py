from panagenda.agenda_platform.infousurpa import InfoUsurpa, w

zones = [{
    'name': 'ZONA DEL BAIX LLOBREGAT – GARRAF - BARCELONÈS',
    'places': [
        'CSO ELS TIMBRES',
        'AT.LLIB.VILADECANS',
        'ATENEU EL PAHTIO'
    ]}, {
    'name': 'GRÀCIA – VALLCARCA – LA SALUT',
    'places': [
        'BLOKES FANTASMA',
        'QUIOSC OKUPAT'
    ]
}
]

places = {
    'CSO ELS TIMBRES': {'name': 'CSO ELS TIMBRES',
     'addr': 'Av.Generalitat, 27 <RENFE> Viladecans',
     'events': {
         w.MON: [{
             'name': 'Assemblea CSO',
             'time': '20h'
         }]
     }},
    'AT.LLIB.VILADECANS': {'name': 'AT.LLIB.VILADECANS',
     'addr': 'C/ Sant Marià 72, <RENFE> Viladecans',
     'events': {
         w.WED: [{
             'name': 'Assemblea',
             'time': '21h30'
         }]
     }},
    'ATENEU EL PAHTIO': {'name': 'ATENEU EL PAHTIO',
     'addr': 'Av.Josep Molins, 22, <M>L5 Pubilla Cases',
     'events': {
         w.MON: [{
             'name': 'Ioga',
             'time': '18h'
         }],
         w.WED: [{
             'name': 'Taller d\'aprenentatge col·lectiu de percussió i musica',
             'time': '18h30'
         },{
             'name': 'Grup acompanyament sanitari',
             'time': '18-20h'
         }]
     }},
    'BLOKES FANTASMA': {'name': 'BLOKES FANTASMA',
     'addr': 'Av. Coll del Portell, 59, <M>L3 Lesseps',
     'events': {
         w.THU: [{
             'name': 'Kafeta',
             'time': '20-24h'
         }],
         w.SAT: [{
             'name': 'Fetish party & Freak Show 3e',
             'time': '20-3h'
         }]
     }},
    'QUIOSC OKUPAT': {'name': 'QUIOSC OKUPAT',
     'addr': 'Quiosc Pl. Revolució, <M>L3 Fontana',
     'events': {
         w.SUN: [{
             'name': 'Distri de fanzines La Polilla',
             'time': '17-21h'
         }]
     }}
}

InfoUsurpa.write_pdf(zones, places, "../resources/infousurpa.pdf")
