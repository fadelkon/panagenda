from panagenda.agenda_platform.activismap import Activismap
from panagenda.panagenda import Activity, Group, Place
from datetime import datetime
import pytz

organizer = Group("Food Not Bombs BCN", "123456")
place = Place("People's saloon", None, Place.Coords("40.482432527705", "3.1002017855644"), "foonix square")
activity = Activity(organizer, place, datetime.now(pytz.utc), datetime.now(pytz.utc),
                    "meal", "mutual aid", "April Food Not Bombs",
                    "Food Not Bombs for 2017 April. Bring anything you want!",
                    None, None, None)

# print(activismap.get_token())
Activismap.write_activities([activity])
#Activismap.write_activity(activity,
#    'NWYyOTBmYmM1MGQwZWYzNGM3NjRiYjgzZjlkYzkzYzI2YzJjZWFkZjI0ZjY1OWI2Mjc4YjAxNjdkYmMzN2FhZg')