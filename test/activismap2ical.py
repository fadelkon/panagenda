from panagenda.agenda_platform.activismap import Activismap
from panagenda.agenda_platform.ical import Icalendar

###########################
# Test activismap to ical #
###########################

a = Activismap.read_activities()
file = open('../resources/activismap-ical.ics', 'wb')
file.write(Icalendar.write_activities(a))
file.close()
