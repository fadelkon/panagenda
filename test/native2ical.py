from panagenda.agenda_platform.ical import Icalendar
from panagenda.panagenda import Activity, Group, Place
from datetime import datetime
import pytz

#######################
# Test native to ical #
#######################

organizer = Group("Food Not Bombs BCN", "123456")
place = Place("square", None)
activity = Activity(organizer, place, datetime.now(pytz.utc), None,
                    "meal", "mutual aid", "April Food Not Bombs",
                    "Food Not Bombs for 2017 April. Bring anything you want!",
                    None, None, None)

file = open('../resources/native-ical.ics', 'wb')
file.write(Icalendar.write_activities([activity]))
file.close()
