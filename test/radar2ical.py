from panagenda.agenda_platform.radar import Radar
from panagenda.agenda_platform.ical import Icalendar

###########################
# Test radar to icalendar #
###########################

radar = Radar()
activities = radar.read_activities()
file = open('../resources/radar-events-bcn.ics', 'wb')
file.write(Icalendar.write_activities(activities))
file.close()
